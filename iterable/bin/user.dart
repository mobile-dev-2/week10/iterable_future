bool anyUserUnder18(Iterable<User> users) {
  return users.any((user) => user.age < 18);
}

bool anyUserOver13(Iterable<User> users) {
  return users.every((user) => user.age > 13);
}

Iterable<User> filterOutUnder21(Iterable<User> users) {
  return users.where((user) => user.age >= 21);
}

Iterable<User> findShortNameed(Iterable<User> user) {
  return user.where((user) => user.name.length <= 3);
}

Iterable<String> getNameAndAges(Iterable<User> users) {
  return users.map((user) => '${user.name} is ${user.age}');
}

void main() {
  var users = [
    User(name: 'ABCDE', age: 14),
    User(name: 'BCDEF', age: 18),
    User(name: 'CDE', age: 18),
    User(name: 'DEF', age: 21),
    User(name: 'E', age: 25)
  ];
  if (anyUserUnder18(users)) {
    print('Have any user under 18');
  }
  if (anyUserOver13(users)) {
    print('Every users over 13');
  }
  var ageMoreThan21User = filterOutUnder21(users);
  for (var user in ageMoreThan21User) {
    print(user.toString());
  }

  var shortNamedUsers = findShortNameed(users);
  for (var user in shortNamedUsers) {
    print(user.toString());
  }

  var nameAndAges = getNameAndAges(users);
  for (var user in nameAndAges) {
    print(user);
  }
}

class User {
  String name;
  int age;
  User({required this.name, required this.age});
  String toString() {
    return '$name , $age';
  }
}
