import 'package:iterable/iterable.dart' as iterable;

void main(List<String> arguments) {
  Iterable<int> iterrable = [1, 2, 3];
  int value = iterrable.elementAt(1);
  print(value);

  const foods = ['Salad', 'Popcorn', 'Toast', 'Lasagne', 'MamaMa'];
  Iterable<String> iterableFoods = foods;
  for (var food in iterableFoods) {
    print(food);
  }

  print('Frist element is ${foods.first}');
  print('Last element is ${foods.last}');

  var foundItem1 = foods.firstWhere((element) => element.length > 5);
  print(foundItem1);
  var foundItem2 = foods.firstWhere((element) => element.length > 5);
  print(foundItem2);

  bool predicate(String item) {
    return item.length > 5;
  }

  var foundItem3 = foods.firstWhere(predicate);
  print(foundItem3);

  var foundItem4 =
      foods.firstWhere((item) => item.length > 10, orElse: () => 'None!');
  print(foundItem4);

  var foundItem5 = foods.firstWhere(
      (item) => item.startsWith('M') && item.contains('a'),
      orElse: () => 'None!');
  print(foundItem5);

  if (foods.any((item) => item.contains('a'))) {
    print('At least on item contrains "a"');
  }

  if (foods.every((item) => item.length >= 5)) {
    print('All item have leght >= 5');
  }

  var numbers = const [1, -2, 3, 42, 0, 4, 5, 6];
  var evenNumber = numbers.where((number) => number.isEven);
  for (var number in evenNumber) {
    print('$number is even');
  }

  if (evenNumber.any((number) => number.isNegative)) {
    print('evenNumber contrins nagative number');
  }

  var largeNumbers = evenNumber.where((number) => number > 1000);
  if (largeNumbers.isEmpty) {
    print('largeNumber is empty');
  }

  var numbersUntilZero = numbers.takeWhile((number) => number != 0);
  print('Number Until 0: $numbersUntilZero');

  var numbersStartingAtZero = numbers.skipWhile((number) => number != 0);
  print('Number Starting at 0: $numbersStartingAtZero');
}
